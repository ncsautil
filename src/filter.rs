use std::collections::HashMap;
use regex::Regex;
use super::Line;

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub enum FilterCmp {
    Ge,
    Gt,
    Le,
    Lt,
    Eq,
    Ne,
}

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub enum FilterStrField {
    Host,
    Ident,
    User,
    Method,
    Path,
    Proto,
    Referer,
    Ua,
}

#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Filter {
    And(Box<Filter>, Box<Filter>),
    Or(Box<Filter>, Box<Filter>),
    Not(Box<Filter>),
    SizeCmp(FilterCmp, u64),
    StatusCmp(FilterCmp, u16),
    Match(FilterStrField, Regex),
    FilterRef(String),
}


// This grammar has some oddities:
// - Negation has somewhat odd precedence: "!size=10" is the same as "size!=10", but
//   "(!size=10&a)" is the same as "(size!=10&a)".
// - Brackets are required around & and | expressions, e.g. "(a & b)" rather than "a & b". On the
//   upside, this simplifies the matter of associativeness. But it does look rather silly to have
//   this rule imposed even on the top-level expression.
peg! filterparse (r#"

cmp -> super::FilterCmp
    = ">=" { super::FilterCmp::Ge }
    / ">"  { super::FilterCmp::Gt }
    / "<=" { super::FilterCmp::Le }
    / "<"  { super::FilterCmp::Lt }
    / "="  { super::FilterCmp::Eq }
    / "!=" { super::FilterCmp::Ne }

strfield -> super::FilterStrField
    = "host"    { super::FilterStrField::Host    }
    / "ident"   { super::FilterStrField::Ident   }
    / "user"    { super::FilterStrField::User    }
    / "method"  { super::FilterStrField::Method  }
    / "path"    { super::FilterStrField::Path    }
    / "proto"   { super::FilterStrField::Proto   }
    / "referer" { super::FilterStrField::Referer }
    / "ua"      { super::FilterStrField::Ua      }

unsigned -> u64
    = [1-9][0-9]* { match_str.parse().unwrap() }
    / "0" { 0u64 }

unit_mult -> u64
    = "G" { 1u64<<30 }
    / "M" { 1u64<<20 }
    / "k" { 1u64<<10 }
    / { 1u64 }

size -> u64
    = u:unsigned m:unit_mult { u * m }

string_part -> &'input str
    = "\\\"" { "\"" }
    / [^"]+  { match_str }

string -> String
    = "\"" s:string_part* "\"" { s.concat() }

regex -> super::super::regex::Regex
    = s:string {? match super::super::regex::Regex::new(&s) { Ok(x) => Ok(x), Err(_) => Err("Invalid regular expression") } }

ident -> &'input str
    = [a-zA-Z0-9_]+ { match_str }

ws
    = [ ]*

#[pub]
filter -> super::Filter
    = [ ]+ filter
    / "(" ws x:filter ")" ws                 { x }
    / "!" ws x:filter                        { super::Filter::Not(Box::new(x)) }
    / "(" ws a:filter "&" ws b:filter ")" ws { super::Filter::And(Box::new(a), Box::new(b)) }
    / "(" ws a:filter "|" ws b:filter ")" ws { super::Filter::Or(Box::new(a), Box::new(b)) }
    / "size"   ws c:cmp ws n:size ws         { super::Filter::SizeCmp(c, n) }
    / "status" ws c:cmp ws n:unsigned ws     { super::Filter::StatusCmp(c, n as u16) }
    / f:strfield ws "~" ws r:regex ws        { super::Filter::Match(f, r) }
    / ":" f:ident ws                         { super::Filter::FilterRef(String::from_str(f)) }

"#);


impl FilterCmp {
    fn apply<T: PartialOrd>(self, a: T, b: T) -> bool {
        match self {
            FilterCmp::Ge => a.ge(&b),
            FilterCmp::Gt => a.gt(&b),
            FilterCmp::Le => a.le(&b),
            FilterCmp::Lt => a.lt(&b),
            FilterCmp::Eq => a.eq(&b),
            FilterCmp::Ne => a.ne(&b),
        }
    }
}


impl FilterStrField {
    fn getfield<'a>(self, l: &'a Line) -> &'a str {
        match self {
            FilterStrField::Host    => l.host,
            FilterStrField::Ident   => l.ident,
            FilterStrField::User    => l.user,
            FilterStrField::Method  => l.method,
            FilterStrField::Path    => l.path,
            FilterStrField::Proto   => l.proto,
            FilterStrField::Referer => l.referer.unwrap_or(""),
            FilterStrField::Ua      => l.ua.unwrap_or(""),
        }
    }
}


impl Filter {
    pub fn new(filter: &str) -> Result<Filter, ()> {
        match filterparse::filter(filter) {
            Ok(x) => Ok(x),
            Err(_) => Err(())
        }
    }

    pub fn apply(&self, line: &Line) -> bool {
        match *self {
            Filter::And(ref a, ref b)   => a.apply(line) && b.apply(line),
            Filter::Or(ref a, ref b)    => a.apply(line) || b.apply(line),
            Filter::Not(ref a)          => !a.apply(line),
            Filter::SizeCmp(c, x)       => c.apply(line.size, x),
            Filter::StatusCmp(c, x)     => c.apply(line.status, x),
            Filter::Match(f, ref r)     => r.is_match(f.getfield(&line)),
            // Can't match without expansion.
            Filter::FilterRef(_)        => unreachable!(),
        }
    }

    /// Recursively expands any FilterRefs.
    pub fn expand(&mut self, lst: &HashMap<String, Filter>) -> Result<(), String> {
        // TODO: Detect and halt on recursion in filters.
        let mut rep = match *self {
            Filter::And(ref mut a, ref mut b) => {
                try!(a.expand(lst));
                try!(b.expand(lst));
                return Ok(())
            },
            Filter::Or(ref mut a, ref mut b) => {
                try!(a.expand(lst));
                try!(b.expand(lst));
                return Ok(())
            },
            Filter::Not(ref mut a) => {
                try!(a.expand(lst));
                return Ok(())
            },
            Filter::FilterRef(ref n) => match lst.get(n) {
                None => return Err(format!("unresolved alias: {}", n)),
                Some(x) => x.clone()
            },
            _ => return Ok(()),
        };
        try!(rep.expand(lst));
        *self = rep;
        Ok(())
    }
}


#[test]
fn test_parse() {
    assert_eq!(Filter::new("size > 1"), Ok(Filter::SizeCmp(FilterCmp::Gt, 1)));
    assert_eq!(Filter::new("size < 1G"), Ok(Filter::SizeCmp(FilterCmp::Lt, 1024*1024*1024)));
    assert_eq!(Filter::new(" (  status  >=  1   &  status<=10) "),
        Ok(Filter::And(
            Box::new(Filter::StatusCmp(FilterCmp::Ge, 1)),
            Box::new(Filter::StatusCmp(FilterCmp::Le, 10))
        ))
    );
}
