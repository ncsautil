use ::std;
use ::time;

pub struct Line<'a> {
    pub host: &'a str,
    pub ident: &'a str,
    pub user: &'a str,
    pub tm: &'a str,
    pub method: &'a str,
    pub path: &'a str,
    pub proto: &'a str,
    pub status: u16,
    pub size: u64,
    pub referer: Option<&'a str>,
    pub ua: Option<&'a str>,
}


// Note that this parser operates on bytes rather than characters. This is fine, since we're
// basically just splitting the string on ASCII characters, all multibyte UTF-8 (>0x80) bytes are
// ignored.
//
// Also, this parser is ugly as hell.
//
// TODO: Proper Error returns.
struct Parser<'a> {
    orig: &'a str,
    iter: std::str::Bytes<'a>,
    curpos: usize,
    lastpos: usize,
    eof: bool,
}


impl<'a> Parser<'a> {
    fn new(orig: &str) -> Parser {
        Parser { orig: orig, iter: orig.bytes(), curpos: 0, lastpos: 0, eof: false }
    }

    fn token(&mut self) -> &'a str {
        let ret = &self.orig[self.lastpos..self.curpos];
        self.lastpos = self.curpos + 1;
        ret
    }

    fn uptochar(&mut self, ch: u8, eofok: bool) -> Result<&'a str, ()> {
        loop {
            match self.iter.next() {
                None => {
                    self.eof = true;
                    return if eofok { Ok(self.token()) } else { Err(()) };
                },
                Some(c) => {
                    if c == ch {
                        let t = self.token();
                        self.curpos += 1;
                        return Ok(t);
                    }
                }
            }
            self.curpos += 1;
        }
    }

    fn consume(&mut self, ch: u8) -> Result<(), ()> {
        match self.iter.next() {
            Some(c) if c == ch => {
                self.curpos += 1;
                self.lastpos = self.curpos;
                Ok(())
            },
            _ => Err(())
        }
    }

    fn uptocharu16(&mut self, ch: u8) -> Result<u16, ()> {
        let v = try!(self.uptochar(ch, false));
        match u16::from_str_radix(v, 10) {
            Ok(x) => Ok(x),
            _ => Err(())
        }
    }

    fn uptocharu64(&mut self, ch: u8, eofok: bool) -> Result<u64, ()> {
        let v = try!(self.uptochar(ch, eofok));
        if v == "-" {
            return Ok(0);
        }
        match u64::from_str_radix(v, 10) {
            Ok(x) => Ok(x),
            _ => Err(())
        }
    }

    // For the referer and ua fields.
    fn finalstring(&mut self, eofrequired: bool) -> Result<Option<&'a str>, ()> {
        if self.eof {
            return Ok(None);
        }
        try!(self.consume(34));
        match self.uptochar(34, eofrequired) {
            Err(x) => Err(x),
            Ok(x) => {
                if !eofrequired {
                    // Consume space as well, so that next finalstring() starts at " again.
                    try!(self.consume(32));
                } else if self.iter.next().is_some() { // Ensure that we do have EOF
                    return Err(());
                };
                Ok(Some(x))
            }
        }
    }

    fn parse(&mut self) -> Result<super::Line<'a>, ()> {
        Ok(super::Line {
            host:    try!(self.uptochar(32, false)),
            ident:   try!(self.uptochar(32, false)),
            user:    try!(self.uptochar(32, false)),
            tm:      { try!(self.consume(91)); try!(self.uptochar(93, false)) },
            method:  { try!(self.consume(32)); try!(self.consume(34)); try!(self.uptochar(32, false)) },
            path:    try!(self.uptochar(32, false)),
            proto:   try!(self.uptochar(34, false)),
            status:  { try!(self.consume(32)); try!(self.uptocharu16(32)) },
            size:    try!(self.uptocharu64(32, true)),
            referer: try!(self.finalstring(false)),
            ua:      try!(self.finalstring(true)),
        })
    }
}


impl<'a> Line<'a> {
    pub fn new(line: &str) -> Result<Line, ()> {
        Parser::new(line).parse()
    }

    // Lighttpd by default stores the vhost in the ident field. Some other servers (notably
    // varnishncsa) instead store a full URL in the path field. This function normalizes those two
    // variations into the Lighttpd version: If the path is a full URL, the ident field will be
    // updated to point to the hostname of the URL, and the path field is updated to only list the
    // path component. This function does nothing if the path field isn't a full URL.
    pub fn vhost2ident(&mut self) {
        let start = if self.path.starts_with("http://") { 7 }
            else if self.path.starts_with("https://") { 8 } else { return };
        for (i, c) in (&self.path[start..]).char_indices() {
            if c == '/' {
                self.ident = &self.path[start..i+start];
                self.path = &self.path[i+start..];
                return;
            }
        }
    }

    pub fn parsetm(&self) -> Result<time::Tm, time::ParseError> {
        time::strptime(self.tm, "%d/%b/%Y:%H:%M:%S %z")
    }

    // Write out the formatted log line, excluding trailing newline. (A BufWriter is recommended
    // for performance).
    pub fn write(&self, w: &mut std::io::Write) -> std::io::Result<()> {
        // Does a write!() allocate? If so, we should avoid it.
        try!(write!(w, "{} {} {} [{}] \"{} {} {}\" {} {}",
               self.host, self.ident, self.user, self.tm, self.method, self.path, self.proto, self.status, self.size));
        match (self.referer, self.ua) {
            (Some(r), Some(u)) => try!(write!(w, " \"{}\" \"{}\"", r, u)),
            _ => (),
        }
        Ok(())
    }
}




#[test]
fn test_extended() {
    let line = match Parser::new("host ident user [da te] \"method path proto\" 200 123456789 \"refe rer\" \"u a\"").parse() {
        Ok(x) => x,
        _ => { assert!(false); return }
    };
    assert_eq!(line.host, "host");
    assert_eq!(line.ident, "ident");
    assert_eq!(line.user, "user");
    assert_eq!(line.tm, "da te");
    assert_eq!(line.method, "method");
    assert_eq!(line.path, "path");
    assert_eq!(line.proto, "proto");
    assert_eq!(line.status, 200);
    assert_eq!(line.size, 123456789);
    assert_eq!(line.referer, Some("refe rer"));
    assert_eq!(line.ua, Some("u a"));
}


#[test]
fn test_common() {
    let line = match Parser::new("host ident user [date] \"method path proto\" 200 123456789").parse() {
        Ok(x) => x,
        _ => { assert!(false); return }
    };
    assert_eq!(line.host, "host");
    assert_eq!(line.ident, "ident");
    assert_eq!(line.user, "user");
    assert_eq!(line.tm, "date");
    assert_eq!(line.method, "method");
    assert_eq!(line.path, "path");
    assert_eq!(line.proto, "proto");
    assert_eq!(line.status, 200);
    assert_eq!(line.size, 123456789);
    assert_eq!(line.referer, None);
    assert_eq!(line.ua, None);
}

#[test]
fn test_invalid() {
    let invalid = &[
        "",
        "host ",
        "host ident ",
        "host ident user",
        "host ident user [date] ",
        "host ident user [date] \"method path proto\" 200 ",
        "host ident user [date] \"method path proto\" 200 1 ",
        "host ident user [date] \"method path proto' 200 1",
        "host ident user [date] 'method path proto' 200 1",
        "host ident user [date \"method path proto\" 200 1",
        "host ident user date \"method path proto\" 200 1",
        "host ident user [date] \"method path proto\" 200 1 \"abc\"",
        "host ident user [date] \"method path proto\" 200 1 \"ref\" \"ua\" ",
    ];
    for s in invalid {
        assert!(Parser::new(s).parse().is_err(), "{}", s);
    }
}


#[test]
fn test_vhost2ident() {
    let mut l = Line::new("host - user [tm] \"method http://blicky.net/ proto\" 200 1").unwrap();
    l.vhost2ident();
    assert_eq!(l.ident, "blicky.net");
    assert_eq!(l.path, "/");

    let mut l = Line::new("host - user [tm] \"method https://whatever.blicky.net/Longer_path?with_stuff proto\" 200 1").unwrap();
    l.vhost2ident();
    assert_eq!(l.ident, "whatever.blicky.net");
    assert_eq!(l.path, "/Longer_path?with_stuff");
}


#[bench]
fn bench_extended(b: &mut super::test::Bencher) {
    b.iter(|| {
        let _ = Parser::new("host ident user [da te] \"method path proto\" 200 123456789 \"refe rer\" \"u a\"").parse();
    });
}
