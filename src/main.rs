#![feature(test)]
#![feature(plugin,collections,str_char)] // For peg
#![plugin(peg_syntax_ext)]

extern crate test;
extern crate time;
extern crate getopts;
extern crate regex;

mod line;
mod filter;
mod config;

use std::io::{stdin,stdout,stderr,Write,BufRead};

pub use line::Line;
use config::Config;


fn main() {
    let mut c = Config::new();
    let filter = c.parse_args();

    let s = stdin();
    let o = stdout();
    let mut o = o.lock();
    for l in s.lock().lines().map(|x| x.unwrap()) {
        let mut line = match Line::new(l.as_ref()) {
            Ok(x) => x,
            Err(_) => {
                if !c.quiet {
                    writeln!(&mut stderr(), "Unrecognized line: {}", l).unwrap();
                }
                continue;
            }
        };

        if c.vhost2ident {
            line.vhost2ident();
        }

        if filter.apply(&line) {
            line.write(&mut o).unwrap();
            o.write_all("\n".as_bytes()).unwrap();
        }
    }
}
